package com.example.appmahfa.mahfa.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appmahfa.mahfa.Models.ChannelModel;
import com.example.appmahfa.mahfa.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Asus on 9/3/2016.
 */

public class ChannelAdapter extends BaseAdapter {

    Context mcontext;
    List<ChannelModel> channels;

    public ChannelAdapter(Context mcontext, List<ChannelModel> channels) {
        this.mcontext = mcontext;
        this.channels = channels;

    }

    @Override
    public int getCount() {
        return channels.size();
    }

    @Override
    public Object getItem(int i) {
        return channels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mcontext).inflate(R.layout.channel_grid_item, viewGroup, false);

        ImageView Logo = (ImageView) rowView.findViewById(R.id.Logo);
        TextView channelName = (TextView) rowView.findViewById(R.id.channelName);
        channelName.setText(channels.get(i).getChannelName());
        Picasso.with(mcontext).load(channels.get(i).getChannelLogo()).into(Logo);
        return rowView;
    }

}

