package com.example.appmahfa.mahfa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.appmahfa.mahfa.adapters.ChannelListAdapter;

public class ListViewActivity extends AppCompatActivity {
    ListView channelList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        channelList =(ListView)findViewById(R.id.channelsList);
        String channelsTV[]={"شبکه یک","شبکه دو","شبکه سه","شبکه چهار","شبکه پنج","شبکه شش","شبکه افق","شبکه آی فیلم","شبکه نسیم","شبکه مستند"};

        ChannelListAdapter adapter= new ChannelListAdapter(this ,channelsTV);
        channelList.setAdapter(adapter);
        channelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String valueClicked=(String) adapterView.getItemAtPosition(position);
            }
        });

    }

}
