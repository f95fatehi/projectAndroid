package com.example.appmahfa.mahfa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appmahfa.mahfa.Models.AdhanModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class AdhanActivity extends AppCompatActivity implements View.OnClickListener {

    TextView sunSet, sunRise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oghat);

        sunSet = (TextView) findViewById(R.id.sunset);
        sunRise = (TextView) findViewById(R.id.sunRise);
        findViewById(R.id.btnShow).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                getDataFromAPI();
//            }
//        }).start();
        GetDataByAsyncHttp();
    }

    void GetDataByAsyncHttp()
    {
        AsyncHttpClient client = new AsyncHttpClient();
        String GET_URL =
                "http://api.aladhan.com/timings/1398332113?latitude=51.508515&longitude=-0.1254872&timezonestring=Europe/London&method=2";
        client.get(GET_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(AdhanActivity.this,"***error***"+throwable,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndSetData(responseString);
            }
        });

    }

    private void getDataFromAPI() {
        try {
            String GET_URL =
                    "http://api.aladhan.com/timings/1398332113?latitude=51.508515&longitude=-0.1254872&timezonestring=Europe/London&method=2";
            URL obj = new URL(GET_URL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        parseAndSetData(response.toString());
                    }
                });
            }

        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(AdhanActivity.this, "error is connecting", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    void parseAndSetData(String jsonSTR) {
        Gson gson = new Gson();
        AdhanModel model = gson.fromJson(jsonSTR, AdhanModel.class);
        sunRise.setText(model.getData().getTimings().getSunrise());
        sunSet.setText(model.getData().getTimings().getSunset());

//        try {
//            JSONObject allObj = new JSONObject(jsonSTR);
//            String dataStr = allObj.getString("data");
//            JSONObject dataObj = new JSONObject(dataStr);
//
//            String timingSTR = dataObj.getString("timings");
//            JSONObject timingsObj = new JSONObject(timingSTR);
//            String sunrise = timingsObj.getString("Sunrise");
//            String sunset = timingsObj.getString("Sunset");
//
//
//
//            sunRise.setText(sunrise);
//            sunSet.setText(sunset);
//
//        } catch (Exception e) {
//
//                    Toast.makeText(AdhanActivity.this, "error ", Toast.LENGTH_LONG).show();
//        }
    }
}


