package com.example.appmahfa.mahfa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appmahfa.mahfa.Models.WeatherModel;
import com.example.appmahfa.mahfa.adapters.ChannelListAdapter;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener {

    TextView windDirection,windChill,windSpeed;
    TextView distance,speed,pressure,temperature;
        EditText city;
        String valueCity="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);


        windSpeed=(TextView)findViewById(R.id.windSpeed);
        windDirection=(TextView)findViewById(R.id.windDirection);
        windChill=(TextView)findViewById(R.id.windChill);
        distance=(TextView)findViewById(R.id.Distance);
        speed=(TextView)findViewById(R.id.speed);
        pressure=(TextView)findViewById(R.id.pressure);
        temperature=(TextView)findViewById(R.id.temperature);
        findViewById(R.id.weatherDetialShow).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        city=(EditText)findViewById(R.id.city);
        valueCity=city.getText().toString().toUpperCase();
        GetDataByAsyncHttpForWeather();
    }

    private void GetDataByAsyncHttpForWeather() {
        AsyncHttpClient clientWeather= new AsyncHttpClient();

       String GET_URL_weather="https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
               valueCity+ "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        clientWeather.get(GET_URL_weather, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(WeatherActivity.this,"***error***"+throwable,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndSetDataWeather(responseString);
            }
        });


    }

    private void parseAndSetDataWeather(String responseString) {
        Gson gsonWeather=new Gson();
        WeatherModel modelWeather = gsonWeather.fromJson(responseString,WeatherModel.class);
        windChill.setText(modelWeather.getQuery().getResults().getChannel().getWind().getChill());
        windDirection.setText(modelWeather.getQuery().getResults().getChannel().getWind().getDirection());
        windSpeed.setText(modelWeather.getQuery().getResults().getChannel().getWind().getSpeed());
        distance.setText(modelWeather.getQuery().getResults().getChannel().getUnits().getDistance());
        speed.setText(modelWeather.getQuery().getResults().getChannel().getUnits().getSpeed());
        temperature.setText(modelWeather.getQuery().getResults().getChannel().getUnits().getTemperature());
        pressure.setText(modelWeather.getQuery().getResults().getChannel().getUnits().getPressure());
    }
}
