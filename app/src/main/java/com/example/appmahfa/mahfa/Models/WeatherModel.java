
package com.example.appmahfa.mahfa.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherModel {

    @SerializedName("query")
    @Expose
    private Query query;

    /**
     * No args constructor for use in serialization
     * 
     */
    public WeatherModel() {
    }

    /**
     * 
     * @param query
     */
    public WeatherModel(Query query) {
        super();
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

}
