package com.example.appmahfa.mahfa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowBatteryActivity extends AppCompatActivity {

    TextView batteryLevel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_battery);

        IntentFilter batteryFilter=new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(PlugOnBattery,batteryFilter);


//        this.registerReceiver(this.PlugOnBattery, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
//        Intent startShowBatteryActivity = getIntent();

    }

    private BroadcastReceiver PlugOnBattery = new BroadcastReceiver() {
        private int level;

        String batteryLevelString;
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            batteryLevel=(TextView)findViewById(R.id.batteryLevel);
            level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            batteryLevelString= String.valueOf(level);
            batteryLevel.setText("%"+batteryLevelString);
        }
    };

};






