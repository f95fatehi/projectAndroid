package com.example.appmahfa.mahfa.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appmahfa.mahfa.R;

/**
 * Created by Asus on 9/2/2016.
 */

public class ChannelListAdapter extends BaseAdapter{
    Context mContext;
    String channels[];

    public ChannelListAdapter(Context mContext, String[] channels) {
        this.mContext = mContext;
        this.channels = channels;
    }

    @Override
    public int getCount() {
        return channels.length;
    }

    @Override
    public Object getItem(int position) {
        return channels[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.channel_list_item,viewGroup, false);
        TextView channelName=(TextView) rowView.findViewById(R.id.channelName);
        channelName.setText(channels[position]);

        return rowView;
    }
}
