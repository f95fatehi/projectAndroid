package com.example.appmahfa.mahfa.adapters;

import android.content.Context;
import android.preference.PreferenceManager;
import android.widget.Toast;


public class PublicMethods {
    Context mContext;

    public PublicMethods(Context mContext) {
        this.mContext = mContext;
    }

    void setShared(String name, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit()
                .putString(name, value).apply();
    }

    String getShared(String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString(key, defaultValue);
    }

    void showToast(String str) {
        Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context mContext, String str) {
        Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
    }


}
