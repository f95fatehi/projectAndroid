package com.example.appmahfa.mahfa.Models;

/**
 * Created by Asus on 9/3/2016.
 */

public class ChannelModel {
    String channelName;
    int channelID;
    String channelLogo;
    Boolean isFree;

    public ChannelModel() {
    }

    public ChannelModel(String channelModel, int channelID, String channelLogo, Boolean isFree) {
        this.channelName = channelName;
        this.channelID = channelID;
        this.channelLogo = channelLogo;
        this.isFree = isFree;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getChannelID() {
        return channelID;
    }

    public void setChannelID(int channelID) {
        this.channelID = channelID;
    }

    public String getChannelLogo() {
        return channelLogo;
    }

    public void setChannelLogo(String channelLogo) {
        this.channelLogo = channelLogo;
    }

    public Boolean getFree() {
        return isFree;
    }

    public void setFree(Boolean free) {
        isFree = free;
    }

    @Override
    public String toString() {
        return "ChannelModel{" +
                "channelName='" + channelName + '\'' +
                ", channelID=" + channelID +
                ", channelLogo='" + channelLogo + '\'' +
                ", isFree=" + isFree +
                '}';
    }
}

