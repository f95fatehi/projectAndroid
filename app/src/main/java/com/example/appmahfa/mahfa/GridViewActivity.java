package com.example.appmahfa.mahfa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import com.example.appmahfa.mahfa.Models.ChannelModel;
import com.example.appmahfa.mahfa.adapters.ChannelAdapter;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends AppCompatActivity {

    GridView channelsGrid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        channelsGrid =(GridView) findViewById(R.id.channelsGrid);

        ChannelModel channel1=new ChannelModel();
        channel1.setChannelID(1);
        channel1.setChannelName("apple");
        channel1.setChannelLogo("http://www.iribtv.ir/files/iribtvnew/mousehover/m01.png");


        ChannelModel channel2=new ChannelModel();
        channel2.setChannelID(2);
        channel2.setChannelName("peach");
        channel2.setChannelLogo("https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.safand.com%2Fupload%2Fnews%2Fimage%2Flarge%2F2014%2F02%2Fimage_5300e4531921f.jpg&imgrefurl=https%3A%2F%2Fwww.safand.com%2Fblog%2Fstory%2F%25D9%2587%25D9%2584%25D9%2588&docid=uVzPTqwCWdWCxM&tbnid=7dnuKJCHLGO-FM%3A&vet=10ahUKEwjR05b4u4HVAhWDHpoKHfEzBCcQMwgxKBEwEQ..i&w=800&h=800&bih=763&biw=1536&q=%D9%87%D9%84%D9%88&ved=0ahUKEwjR05b4u4HVAhWDHpoKHfEzBCcQMwgxKBEwEQ&iact=mrc&uact=8.png");

        ChannelModel channel3=new ChannelModel();
        channel3.setChannelID(3);
        channel3.setChannelName("cucumber");
        channel3.setChannelLogo("https://www.google.com/imgres?imgurl=http%3A%2F%2F7ganj.ir%2Fimg%2F2014%2F07%2F1-15.jpg&imgrefurl=http%3A%2F%2F7ganj.ir%2F%25D8%25A2%25D8%25B4%25D9%2586%25D8%25A7%25D9%258A%25D9%258A-%25D8%25A8%25D8%25A7-%25D8%25AE%25D9%2588%25D8%25A7%25D8%25B5-%25D9%2588-%25D9%2585%25D8%25B6%25D8%25B1%25D8%25A7%25D8%25AA-%25D8%25AE%25D9%258A%25D8%25A7%25D8%25B1%2F&docid=lFys1YUFpdwZUM&tbnid=6X34lQnDd5tusM%3A&vet=10ahUKEwjR5OWjvIHVAhXKNpoKHbG5AS8QMwgkKAQwBA..i&w=600&h=427&bih=763&biw=1536&q=%D8%AE%DB%8C%D8%A7%D8%B1&ved=0ahUKEwjR5OWjvIHVAhXKNpoKHbG5AS8QMwgkKAQwBA&iact=mrc&uact=8.png");

        ChannelModel channel4=new ChannelModel();
        channel4.setChannelID(4);
        channel4.setChannelName("black cherry");
        channel4.setChannelLogo("https://www.google.com/imgres?imgurl=http%3A%2F%2Fimg.tebyan.net%2Fbig%2F1388%2F12%2F18142712002253080196196241182222100250132.jpg&imgrefurl=http%3A%2F%2Farticle.tebyan.net%2F119193%2F%25D8%25A2%25D9%2584%25D8%25A8%25D8%25A7%25D9%2584%25D9%2588-%25D8%25AE%25D9%2588%25D8%25B4-%25D9%2585%25D8%25B2%25D9%2587-%25D8%25B2%25DB%258C%25D8%25A8%25D8%25A7-%25D9%2585%25D9%2581%25DB%258C%25D8%25AF&docid=tYr1X0tLCwsBlM&tbnid=lQwJDtJrOzl0UM%3A&vet=10ahUKEwiNjqDZvIHVAhUFApoKHaKcC4sQMwgsKAwwDA..i&w=180&h=210&bih=763&biw=1536&q=%D8%A2%D9%84%D8%A8%D8%A7%D9%84%D9%88&ved=0ahUKEwiNjqDZvIHVAhUFApoKHaKcC4sQMwgsKAwwDA&iact=mrc&uact=8.png");

        ChannelModel channel5=new ChannelModel();
        channel5.setChannelID(1);
        channel5.setChannelName("watermelon");
        channel5.setChannelLogo("https://www.google.com/imgres?imgurl=http%3A%2F%2Fwww.pvfarms.com%2Fimages%2Fourproduce_watermelon.png&imgrefurl=http%3A%2F%2Fwww.pvfarms.com%2Fwatermelon.php&docid=MWKHIX23O8LxNM&tbnid=vZwu1hWHFt-REM%3A&vet=10ahUKEwj8xtqmvYHVAhWmCJoKHRZmBLcQMwjyASgQMBA..i&w=436&h=405&bih=763&biw=1536&q=watermelon&ved=0ahUKEwj8xtqmvYHVAhWmCJoKHRZmBLcQMwjyASgQMBA&iact=mrc&uact=8.png");

        ChannelModel channel6=new ChannelModel();
        channel6.setChannelID(6);
        channel6.setChannelName("greengage");
        channel6.setChannelLogo("https://www.google.com/imgres?imgurl=http%3A%2F%2Ffiles.namnak.com%2Fusers%2Fhk%2Faup%2F201705%2F201_pics%2F%25DA%25AF%25D9%2588%25D8%25AC%25D9%2587-%25D8%25B3%25D8%25A8%25D8%25B2.jpg&imgrefurl=http%3A%2F%2Fnamnak.com%2F%25DA%25AF%25D9%2588%25D8%25AC%25D9%2587-%25D8%25B3%25D8%25A8%25D8%25B2-%25D8%25AF%25D8%25B1-%25D8%25AF%25D9%2588%25D8%25B1%25D8%25A7%25D9%2586-%25D8%25A8%25D8%25A7%25D8%25B1%25D8%25AF%25D8%25A7%25D8%25B1%25DB%258C.p48715&docid=8IlvqCP_5rhbZM&tbnid=_mhZT970kW7pvM%3A&vet=10ahUKEwjz4ZC2vYHVAhXCDZoKHaHVBw8QMwggKAAwAA..i&w=500&h=500&bih=763&biw=1536&q=%DA%AF%D9%88%D8%AC%D9%87%20%D8%B3%D8%A8%D8%B2&ved=0ahUKEwjz4ZC2vYHVAhXCDZoKHaHVBw8QMwggKAAwAA&iact=mrc&uact=8.png");


        List<ChannelModel> channelsFruit=new ArrayList<ChannelModel>();
        channelsFruit.add(channel1);
        channelsFruit.add(channel2);
        channelsFruit.add(channel3);
        channelsFruit.add(channel4);
        channelsFruit.add(channel5);
        channelsFruit.add(channel6);

        ChannelAdapter adapter = new ChannelAdapter(this,channelsFruit);
        channelsGrid.setAdapter(adapter);
    }

}
