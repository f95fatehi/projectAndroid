package com.example.appmahfa.mahfa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.webkit.WebSettings;
import android.widget.Toast;

/**
 * Created by Asus on 7/20/2017.
 */

public class PlugOnBattery extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "battery", Toast.LENGTH_LONG).show();

        Intent startShowBatteryActivity = new Intent();
        startShowBatteryActivity.setClassName("com.example.appmahfa.mahfa","com.example.appmahfa.mahfa.ShowBatteryActivity");
        startShowBatteryActivity.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startShowBatteryActivity);

    }
}
