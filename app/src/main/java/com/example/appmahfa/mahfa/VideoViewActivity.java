package com.example.appmahfa.mahfa;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoViewActivity extends AppCompatActivity {

   VideoView myVideo;
    String VideoURL="http://as7.cdn.asset.aparat.com/aparat-video/1d0a713bef45a0caccb7541969b87f7d7609180-144p__63746.mp4";
    BroadcastReceiver incomingCallReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        checkPrmession();
        myVideo=(VideoView)findViewById(R.id.myVideo);
        myVideo.setMediaController(new MediaController(this));
        myVideo.setVideoURI(Uri.parse(VideoURL));
        myVideo.start();


        incomingCallReceiver =new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(myVideo.isPlaying())
                    myVideo.pause();
            }
        };
        IntentFilter callFilter=new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(incomingCallReceiver,callFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(incomingCallReceiver);
    }

    private void checkPrmession() {
        if (ContextCompat.checkSelfPermission(VideoViewActivity.this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(VideoViewActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE}, 1500);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==1500){

        }
    }
}
