package com.example.appmahfa.mahfa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnListView).setOnClickListener(this);
        findViewById(R.id.btnGridView).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intentListView = new Intent(this, ListViewActivity.class);
        startActivity(intentListView);

        Intent intentGridView = new Intent(this,GridViewActivity.class);
        startActivity(intentGridView);
    }
}
